(function(){
	var app = {
		module: {

			iosFix: {

				removeIOSRubberEffect: function(el) {

					var $body = $('body'),
						t,
						st,
						wh,
						h,
						y0 = 0,
						y1 = 0,
						yd = 0;

					$body
						.on('touchstart', el, function (e) {
							var $t = $(this);

							y0 = e.pageY || e.originalEvent.targetTouches[0].pageY;

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);
						})
						.on('touchmove', el, function (e) {
							var $t = $(this);

							if ( y0 !== 0){
								y1 = e.pageY || e.originalEvent.targetTouches[0].pageY;
								yd = (( y1 - y0 ) > 0 ) ? (-1) : ( ( y1 - y0 ) < 0 ? (1) : 0);
							}

							t = $t.offset().top;
							st = $t.scrollTop();
							wh = $t[0].scrollHeight;
							h = $t.outerHeight(true);

							if ( st <= 0 && yd < 0){
								$t.scrollTop(st);
								e.preventDefault();
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else if ( st >= (wh - h) && yd > 0){
								$t.scrollTop(wh - h);
								$body.on('touchmove.popupbg', function(e){ e.preventDefault(); });
							} else {
								$body.off('touchmove.popupbg');
							}

						})
						.on('touchend', el, function (e) {
							$body.off('touchmove.popupbg');
						});
				},

				init: function(){

					if ( !!Modernizr ){

						Modernizr.addTest('ipad', function () {
							return !!navigator.userAgent.match(/iPad/i);
						}).addTest('iphone', function () {
							return !!navigator.userAgent.match(/iPhone/i);
						}).addTest('ipod', function () {
							return !!navigator.userAgent.match(/iPod/i);
						}).addTest('ios', function () {
							return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
						});
					}

					if ( Modernizr.ipad || Modernizr.ipod || Modernizr.iphone ){

						self.iosFix.removeIOSRubberEffect('.nav');
						self.iosFix.removeIOSRubberEffect('.popup');

					}

				}
			},

			scrollMenu: {

				onScroll: function(el){
					var scrollPos = $(document).scrollTop();

					$(el).each(function () {
						var $t = $(this),
							$pn = $('.js-part-name'),
							$up = $('.nav__legend-up'),
							$down = $('.nav__legend-down'),
							$section = $($t.attr('href'));

						if ( ($section.offset().top - 50 <= scrollPos ) &&  ( $section.offset().top - 50 + $section.height() > scrollPos )) {
							$(el).removeClass('is-active');
							$t.addClass('is-active');

							$pn.text( $t.text() );

						}
						else{
							$t.removeClass('is-active');
						}


						//if ( $t.attr('href') == '#welcome' && !$up.hasClass('is-disabled')){
						//	$up.addClass('is-disabled');
						//} else if ( $t.attr('href') == '#contact' && !$down.hasClass('is-disabled') ){
						//	$down.addClass('is-disabled');
						//} else if ( $down.hasClass('is-disabled') || $up.hasClass('is-disabled') ) {
						//	$up.removeClass('is-disabled');
						//	$down.removeClass('is-disabled');
						//}
					});
				},

				init: function(el){
					$(window).on('scroll', function(){
						self.scrollMenu.onScroll(el);
					});

					self.scrollMenu.onScroll(el);
				}
			},

			checkForm: function(form){
				var $el = $(form),
					wrong = false;

				$el.find('[data-required]').each(function(){
					var $t = $(this),
						type = $t.data('required'),
						$wrap = $t.closest('.form-field'),
						$mes = $wrap.find('.form-field__message'),
						val = $.trim($t.val()),
						errMes = '',
						rexp = /.+$/igm;

					$wrap.removeClass('error').removeClass('success');
					$mes.html('');

					if ( $t.attr('type') == 'checkbox' && !$t.is(':checked') ) {
						val = false;
					} else if ( /^(#|\.)/.test(type) ){
						if ( val !== $(type).val() || !val ) val = false;
					} else if ( /^(name=)/.test(type) ){
						if ( val !== $('['+type+']').val() || !val ) val = false;
					} else if ( $t.attr('type') == 'radio'){
						var name =  $t.attr('name');
						if ( $('input[name='+name+']:checked').length < 1 ) val = false;
					} else {
						switch (type) {
							case 'number':
								rexp = /^\d+$/i;
								errMes = 'Поле должно содержать только числовые символы';
								break;
							case 'phone':
								rexp = /[\+]\d\s[\(]\d{3}[\)]\s\d{3}\s\d{2}\s\d{2}/i;
								break;
							case 'letter':
								rexp = /^[A-zА-яЁё]+$/i;
								break;
							case 'rus':
								rexp = /^[А-яЁё]+$/i;
								break;
							case 'email':
								rexp = /^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/i;
								errMes = 'Проверьте корректность email';
								break;
							case 'password':
								rexp = /^(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/;
								errMes = 'Слишком простой пароль';
								break;
							default:
								rexp = /.+$/igm;
						}
					}


					if ( !rexp.test(val) || val == 'false' || !val ){
						wrong = true;
						$wrap.addClass('error');

						if (!val) errMes = 'Поле не заполнено';

						if ( errMes ){

							$mes.html(errMes);
						}

					} else {
						$wrap.addClass('success');

					}

					function setMessage(text) {

					}

				});


				return !wrong;

			},

			tab: {
				animateSlide: false,
				class: 'is-active',

				init: function(){

					$('body').on('click', '[data-tab-link]', function(e){
						/**
						 * data-tab-link='name_1', data-tab-group='names'
						 * data-tab-targ='name_1', data-tab-group='names'
						 **/
						e.preventDefault();
						var $t = $(this);
						var group = $t.data('tab-group');
						var $links = $('[data-tab-link]').filter(selectGroup);
						var $tabs = $('[data-tab-targ]').filter(selectGroup);
						var ind = $t.data('tab-link');
						var $tabItem = $('[data-tab-targ='+ind+']').filter(selectGroup);

						if( !$t.hasClass(self.tab.class)){
							$links.removeClass(self.tab.class);
							$t.addClass(self.tab.class);

							if ( self.tab.animateSlide ){
								$tabs.fadeOut(150);
								setTimeout(function(){
									$tabs.removeClass(self.tab.class);
									$tabItem.fadeIn(150, function(){
										$(this).addClass(self.tab.class);
									});
								}, 150)
							} else {
								$tabs.removeClass(self.tab.class);
								$tabItem.addClass(self.tab.class);
							}
						}

						function selectGroup(){
							return $(this).data('tab-group') === group;
						}
					});

				}
			},

			popup: {
				create: function(popup){
					var pref = popup.indexOf('#') == 0  ? 'id' : 'class';
					var name = popup.replace(/^[\.#]/,'');
					var $popup = $('<div class="popup">'
						+			'<div class="popup-inner">'
						+				'<div class="popup-layout">'
						+					'<div class="popup-close"></div>'
						+					'<div class="popup-content"></div>'
						+				'</div>'
						+			'</div>'
						+			'<div class="popup-overlay"></div>'
						+		'</div>').appendTo('body');

					if ( pref == 'id'){
						$popup.attr(pref, name);
					} else {
						$popup.addClass(name);
					}

					return $popup;
				},

				open: function(popup, html){
					var $popup = $(popup);
					if (!$popup.size()){
						$popup = self.popup.create(popup);
					}
					if( html ){
						$popup.find('.popup-content').html(html);
					}
					$('body').addClass('overflow_hidden');
					return $popup.show();
				},

				close: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.hide();
				},

				info: function(mes, callback){
					var html = '<div class="popup-text">' + mes + '</div>'
						+	'<div class="popup-link"><span class="btn btn_green">Ок</span></div>';
					self.popup.open('.popup_info', html);

					$('.popup_info').find('.btn').click(function(){
						self.popup.close($('.popup_info'));
						if ( callback ) callback();
					});
				},

				remove: function(popup){
					var $popup = $(popup);
					$('body').removeClass('overflow_hidden');
					$popup.remove();
				}
			},

			scrollBar: {

				init: function(el){
					$(el).not('.inited').mCustomScrollbar({
						callbacks:{
							onCreate: function(){
								$(this).addClass('inited');
							}
						}
					});
				}
			}

		},
		init: function() {

			self.tab.init();
			self.scrollMenu.init('.nav__link');



			var $b = $('body');

			$b
				.on('click', '.scrollto', function(e){
					e.preventDefault();
					var $this = $(this.hash);
					$('html,body').animate({scrollTop:$this.offset().top}, 500);
				})
				.on('click', '.js-popup-course-login', function (e) {
					e.preventDefault();
					var $this = $(this);
					$this.closest('.popup-header').toggleClass('popup-header_before popup-header_after');
				})
				.on('click', '[data-popup]', function(e){
					e.preventDefault();
					e.stopPropagation();
					self.popup.close('.popup');
					self.popup.open($(this).data('popup'));
				})
				// popup close
				.on('click', '.popup', function(e){
					if ( !$(e.target).closest('.popup-layout').size() ) self.popup.close('.popup');
				})
				.on('click', '.popup-close', function(e){
					e.preventDefault();
					self.popup.close('.popup');
				})

				.on('click', '.nav__handler', function(e){
					e.preventDefault();
					$b.toggleClass('show-menu');
				})

				.on('click', '.nav__link', function(e){
					$b.removeClass('show-menu');
					self.popup.close('.popup');

					$('html,body').animate({scrollTop:$(this.hash).offset().top}, 300);
				})

				.on('click', '.nav__legend-up', function(e){
					var prev = $('.nav__link.is-active').closest('.nav__item').prev().find('.nav__link').attr('href');
					if ( prev !== undefined ) $('html,body').animate({scrollTop:$(prev).offset().top}, 300);
				})

				.on('click', '.nav__legend-down', function(e){
					var next = $('.nav__link.is-active').closest('.nav__item').next().find('.nav__link').attr('href');
					if ( next !== undefined ) $('html,body').animate({scrollTop:$(next).offset().top}, 300);
				})

				.on('submit', '.js-form', function (e) {
					e.preventDefault();
					var $form = $(this);

					if (self.checkForm($form)) {

						$.ajax({
							type: $form.attr('method'),
							url: $form.attr('action'),
							data: data,
							complete: function (res) {

								var data = JSON.parse(res.responseText);

								if (res.status) {
									var status = res.status,
										formName = $form.data('form-name');

									if (status == 200) {    /** success */

										switch (formName) {
											case 'feedback':

												break;

											case 'faq':

												break;
										}

										location.reload();

									} else if (status == 400) {    /** error */

									} else if (status == 500) {    /** Internal Server Error */

									} else {
										/** other trouble */
										console.error(res);
									}

								}
							}
						});
					}

				})

			;


			/**
			 * carousel
			 */

			/**
			 * welcome carousel
			 */

			$('.carousel_welcome').slick({

				dots: true,
				infinite: true,
				speed: 300,
				slidesToShow: 2,
				centerMode: true,
				vertical: true,
				swipe: false,
				touchMove: false,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_v" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_v" aria-label="Next" tabindex="0" role="button"></button>',
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							centerMode: false,
							swipe: true,
							touchMove: true,
							slidesToShow: 1,
							vertical: false,
							prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
							nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>',
						}
					}
				]
			});


			/**
			 * announce carousel
			 */

			$('.carousel_announce').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				centerMode: false,
				vertical: false,
				touchMove: true,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>'
			});


			/**
			 * prospect carousel
			 */

			$('.carousel_prospect').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				centerMode: false,
				vertical: false,
				touchMove: true,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>'
			});


			/**
			 * about carousel
			 */

			$('.carousel_about').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				centerMode: false,
				vertical: false,
				touchMove: true,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>'
			});

			/**
			 * feedback carousel
			 */

			$('.carousel_feedback-photo').slick({
				dots: false,
				asNavFor: '.carousel_feedback-text',
				infinite: true,
				speed: 300,
				slidesToShow: 1,
				vertical: false,
				touchMove: false,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>',
				centerMode: true,
				variableWidth: true,
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							centerMode: false,
							variableWidth: false
						}
					}
				]

			});

			$('.carousel_feedback-text').slick({
				dots: false,
				asNavFor: '.carousel_feedback-photo',
				infinite: true,
				adaptiveHeight: true,
				speed: 300,
				slidesToShow: 1,
				centerMode: false,
				vertical: false,
				touchMove: true,
				prevArrow: '<button type="button" data-role="none" class="slick-prev slick-prev_h" aria-label="Prev" tabindex="0" role="button"></button>',
				nextArrow: '<button type="button" data-role="none" class="slick-next slick-next_h" aria-label="Next" tabindex="0" role="button"></button>'
			});




		}
	};
	var self = {};
	var loader = function(){
		self = app.module;
		jQuery.app = app.module;
		app.init();
	};
	var ali = setInterval(function(){
		if (typeof jQuery !== 'function') return;
		clearInterval(ali);
		setTimeout(loader, 0);
	}, 50);

})();



