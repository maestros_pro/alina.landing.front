(function() {
	"use strict";

	function uaMatch( ua ) {
		// If an UA is not provided, default to the current browser UA.
		if ( ua === undefined ) {
			ua = window.navigator.userAgent;
		}
		ua = ua.toLowerCase();

		var match = /(edge)\/([\w.]+)/.exec( ua ) ||
			/(yabrowser)[\/]([\w.]+)/.exec( ua ) ||
			/(firefox)[\/]([\w.]+)/.exec( ua ) ||
			/(opr)[\/]([\w.]+)/.exec( ua ) ||
			/(chrome)[ \/]([\w.]+)/.exec( ua ) ||
			/(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
			/(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
			/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
			/(opera mini)[ \/]([\w.]+)/.exec( ua ) ||
			/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
			/(msie) ([\w.]+)/.exec( ua ) ||
			ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
			ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
			[];

		var platform_match = /(ipad)/.exec( ua ) ||
			/(ipod)/.exec( ua ) ||
			/(iphone)/.exec( ua ) ||
			/(kindle)/.exec( ua ) ||
			/(silk)/.exec( ua ) ||
			/(android)/.exec( ua ) ||
			/(windows phone)/.exec( ua ) ||
			/(win)/.exec( ua ) ||
			/(mac)/.exec( ua ) ||
			/(linux)/.exec( ua ) ||
			/(cros)/.exec( ua ) ||
			/(playbook)/.exec( ua ) ||
			/(bb)/.exec( ua ) ||
			/(blackberry)/.exec( ua ) ||
			[];

		var browser = {},
			matched = {
				browser: match[ 5 ] || match[ 3 ] || match[ 1 ] || "",
				version: match[ 2 ] || match[ 4 ] || "0",
				versionNumber: match[ 4 ] || match[ 2 ] || "0",
				platform: platform_match[ 0 ] || ""
			};

		if ( matched.browser ) {
			browser[ matched.browser ] = true;
			browser.version = matched.version;
			browser.versionNumber = parseInt(matched.versionNumber, 10);
		}

		if ( matched.platform ) {
			browser[ matched.platform ] = true;
		}

		// These are all considered mobile platforms, meaning they run a mobile browser
		if ( browser.android || browser.bb || browser.blackberry || browser.ipad || browser.iphone ||
			browser.ipod || browser.kindle || browser.playbook || browser.silk || browser[ "opera mini" ] || browser[ "windows phone" ]) {
			browser.mobile = true;
		}

		// These are all considered desktop platforms, meaning they run a desktop browser
		if ( browser.cros || browser.mac || browser.linux || browser.win ) {
			browser.desktop = true;
		}

		// Chrome, Opera 15+ and Safari are webkit based browsers
		if ( browser.chrome || browser.opr || browser.safari ) {
			browser.webkit = true;
		}

		// IE11 has a new token so we will assign it msie to avoid breaking changes
		// IE12 disguises itself as Chrome, but adds a new Edge token.
		if ( browser.rv || browser.edge ) {
			var ie = "msie";

			//matched.browser = ie;
			//browser[ie] = true;
		}

		// Blackberry browsers are marked as Safari on BlackBerry
		if ( browser.safari && browser.blackberry ) {
			var blackberry = "blackberry";

			matched.browser = blackberry;
			browser[blackberry] = true;
		}

		// Yandex browser
		if ( browser.yabrowser ) {
			matched.browser = "yandex";
		}

		// Playbook browsers are marked as Safari on Playbook
		if ( browser.safari && browser.playbook ) {
			var playbook = "playbook";

			matched.browser = playbook;
			browser[playbook] = true;
		}

		// BB10 is a newer OS version of BlackBerry
		if ( browser.bb ) {
			var bb = "blackberry";

			matched.browser = bb;
			browser[bb] = true;
		}

		// Opera 15+ are identified as opr
		if ( browser.opr ) {
			var opera = "opera";

			matched.browser = opera;
			browser[opera] = true;
		}

		// Stock Android browsers are marked as Safari on Android.
		if ( browser.safari && browser.android ) {
			var android = "android";

			matched.browser = android;
			browser[android] = true;
		}

		// Kindle browsers are marked as Safari on Kindle
		if ( browser.safari && browser.kindle ) {
			var kindle = "kindle";

			matched.browser = kindle;
			browser[kindle] = true;
		}

		// Kindle Silk browsers are marked as Safari on Kindle
		if ( browser.safari && browser.silk ) {
			var silk = "silk";

			matched.browser = silk;
			browser[silk] = true;
		}

		// Assign the name and platform variable
		browser.name = matched.browser;
		browser.platform = matched.platform;

		return browser;

	}

	// Run the matching process, also assign the function to the returned object
	// for manual, jQuery-free use if desired

	window.jQBrowser = uaMatch( window.navigator.userAgent );
//	window.jQBrowser.uaMatch = uaMatch;

	return window.jQBrowser;
})();

$(function(){

	$.browserDetect = function(options){

		var defaults = {
			template: 	'<div class="browser-detect">'
			+				'<div class="browser-detect__overlay"></div>'
			+				'<div class="browser-detect__inner">'
			+					'<div class="browser-detect__title"></div>'
			+					'<div class="browser-detect__subtitle"></div>'
			+					'<div class="browser-detect__message"></div>'
			+					'<div class="browser-detect__browsers">'
			+						'<a href="https://www.google.ru/chrome/browser/desktop/" class="browser-detect__item" target="_blank">Chrome</a> '
			+						'<a href="http://windows.microsoft.com/ru-ru/internet-explorer/download-ie" class="browser-detect__item" target="_blank">Internet Explorer</a> '
			+						'<a href="http://www.opera.com/" class="browser-detect__item" target="_blank">Opera</a> '
			+						'<a href="https://www.mozilla.org/ru/firefox/new/" class="browser-detect__item" target="_blank">Firefox</a> '
			+						( ( jQBrowser.platform == 'mac' || jQBrowser.platform == 'iphone' || jQBrowser.platform == 'ipad' )? '<a href="http://www.apple.com/ru/safari/" class="browser-detect__item" target="_blank">Safari</a> ' : '')
			+					'</div>'
			+			    '</div>'
			+			'</div>',
			testTitle: 'Поздравляем!',
			close: true,    //- close button
			timeout: false,     //- false / hours
			testSubTitle: 'Ваш браузер морально и технически устарел.',
			testMessage: 'Вы можете продолжить смотреть сайт в старом браузере, на свой страх и риск, но мы рекомендуем Вам установить нормальный браузер, например один из этих:',
			check:[

			/**
			 * 		array:
			 * 		1 - browser name (chrome, firefox, opera, opera mini, safari)
			 *		2 - browser version
			 *		3 - os, platform win/mac (optional)
			 *		4 - desktop or mobile view (optional)
			 *		example:
			 */

				['msie', '11', '', 'desktop'],
				['chrome', '42', 'win', 'desktop'],
				['firefox', '40'],
				['opera', '31'],
				['safari', '', 'win'],
				['safari', '8', 'mac']
			],
			browser:{
				name: jQBrowser.name,
				version: jQBrowser.versionNumber,
				platform: jQBrowser.platform,
				view: jQBrowser.desktop ? 'desktop' : ( jQBrowser.mobile ? 'mobile' : null)
			}
		};

		this.config = $.extend({}, defaults, options);

		this.browserCheck = function(){

			var i = 0, cup = false;
			for ( i; i < this.config.check.length; i++ ){
				var browserName = this.config.check[i][0] ==  this.config.browser.name,
					browserVer  = this.config.check[i][1] ? ( this.config.check[i][1] >  this.config.browser.version )  : true,
					browserOs   = this.config.check[i][2] ? ( this.config.check[i][2] == this.config.browser.platform ) : true,
					browserView = this.config.check[i][3] ? ( this.config.check[i][3] == this.config.browser.view )     : true;

				if ( browserName && browserVer && browserOs && browserView ){
					cup = true;
				}
			}

			if ( cup ){
				this.create();
				return true;
			} else {
				return false;
			}

		};

		this.create = function(){
			var $cup = $(this.config.template), that = this;

			var $oldCup = $('.' + $cup.attr('class'));
			if ( $oldCup.size() ) $oldCup.remove();

			$cup.appendTo('body');
			$('body').addClass('browser-old');
			$cup.find('.browser-detect__title').html(that.config.testTitle);
			$cup.find('.browser-detect__subtitle').html(that.config.testSubTitle);
			$cup.find('.browser-detect__message').html(that.config.testMessage);

			if ( that.config.close ){
				$cup.find('.browser-detect__inner').prepend('<div class="browser-detect__close"></div>');
			}

		};

		this.timeout = function(){
			if ( this.config.timeout && supportsLocalStorage() ){

				var ls = +localStorage.getItem('browserCheck');

				if ( !ls ) {
					//- добавляем таймер на отмену показа попапа
					var timeOut = Date.parse(new Date()) + ( this.config.timeout * 3600000 );
					localStorage.setItem('browserCheck', timeOut);
					return true;
				} else {
					return ( ls < Date.parse(new Date()) );
				}

			} else {
				return true;
			}

			function supportsLocalStorage() {
				try {
					return 'localStorage' in window && window['localStorage'] !== null;
				} catch (e) {
					return false;
				}
			}

		};

		this.init = function(){

			$('body').on('click', '.browser-detect__close', function(){
				$(this).closest('.browser-detect').fadeOut();
				$('body').removeClass('browser-old');
			});

			if ( this.timeout() ){
				return this.browserCheck();
			}
		};

		return this.init();
	};

});


